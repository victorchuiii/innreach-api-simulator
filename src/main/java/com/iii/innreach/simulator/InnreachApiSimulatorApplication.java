package com.iii.innreach.simulator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InnreachApiSimulatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(InnreachApiSimulatorApplication.class, args);
	}

}
